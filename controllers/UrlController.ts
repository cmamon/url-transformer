import { FastifyReply, FastifyRequest } from 'fastify';
import faunadb, { Index, Lambda, Map, Match, Paginate, Update } from 'faunadb';
import shortId from 'shortid';
import FaunaError from '../errors/FaunaError';
import { UrlDocument } from '../models/UrlDocument';

const { Create, Collection, Get, Ref } = faunadb.query;

type CreateUrlRequest = FastifyRequest<{
  Body: {
    fullUrl: string,
    customPath?: string,
  };
}>;

type GetUrlRequest = FastifyRequest<{
  Params: { shortPath: string };
}>;

export default class UrlController {
  static faunaClient = new faunadb.Client({
    secret: process.env.FAUNA_SECRET_KEY,
    domain: 'db.eu.fauna.com',
  });

  static async getUrls(request: FastifyRequest, reply: FastifyReply) {
    try {
      const urls = await UrlController.faunaClient.query(
        Map(
          Paginate(Match(Index('urls_index'))),
          Lambda(x => Get(x))
        )
      );

      reply.send(urls);
    } catch (error) {
      throw new FaunaError(error);
    }
  }

  static async createShortUrl(request: CreateUrlRequest, reply: FastifyReply) {
    let shortPathExists = false;
    let shortPath = '';

    do {
      shortPath = request.body.customPath ? request.body.customPath : shortId.generate();

      try {
        const newUrl: UrlDocument = await UrlController.faunaClient.query(
          Create(Collection('Urls'), {
            data: {
              fullUrl: request.body.fullUrl,
              shortPath,
              clicks: 0,
            },
          })
        );

        reply.send(newUrl.data);
      } catch (error) {
        if (error.requestResult) {
          const faunaError = new FaunaError(error);

          if (faunaError.code === 'instance not unique') {
            if (request.body.customPath) {
              reply.send({ message: 'This custom path already exists', statusCode: 409 });
            }

            shortPathExists = true;
            continue;
          }

          throw faunaError;
        }
      }
    } while (shortPathExists);
  }

  static async redirectToFullUrl(request: GetUrlRequest, reply: FastifyReply) {
    try {
      const document: UrlDocument = await UrlController.faunaClient.query(
        Get(Match(Index('url_by_path'), request.params.shortPath))
      );

      await UrlController.faunaClient.query(
        Update(document.ref, { data: { clicks: ++document.data.clicks } })
      );

      reply.redirect(document.data.fullUrl);
    } catch (error) {
      throw new FaunaError(error);
    }
  }
}
