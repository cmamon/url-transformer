export default class UrlControllerSchemas {
  static createShortUrlSchema = {
    schema: {
      body: {
        type: 'object',
        required: ['fullUrl'],
        properties: {
          fullUrl: { type: 'string' },
          customName: { type: 'string' },
        },
      },
    },
  };

  static redirectToFullUrlSchema = {
    schema: {
      params: {
        shortPath: { type: 'string' },
      },
    },
  };
}
