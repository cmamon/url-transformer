import { Url } from './Url';

export interface UrlDocument {
  ref: object;
  ts: number;
  data: Url
}