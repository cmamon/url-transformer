export interface Url {
  fullUrl: string;
  shortUrl: string;
  clicks: number;
}