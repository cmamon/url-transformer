import Fastify, { FastifyInstance } from 'fastify';
import UrlController from './controllers/UrlController';
import UrlControllerSchemas from './controllers/schemas/UrlControllerSchemas';

const server: FastifyInstance = Fastify();

const port = 5000;

server.get('/', UrlController.getUrls);

server.post(
  '/create-short-url',
  UrlControllerSchemas.createShortUrlSchema,
  UrlController.createShortUrl
);

server.get(
  '/:shortPath',
  UrlControllerSchemas.redirectToFullUrlSchema,
  UrlController.redirectToFullUrl
);

const start = async () => {
  try {
    server.listen(port, () => {
      return console.log(`server is listening on ${port}`);
    });
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};

start();
